import '@babel/polyfill';
let news;
var count_vote = 0;

document.addEventListener('DOMContentLoaded', () => {
   news = document.getElementById('recent_links');
   let search = document.getElementById('search');
   
   search.addEventListener('keyup', (e)=>{
      if(e.key == 'Enter'){
         getNews2(search.value)
      }
   })
   getNews('iraq');
})


function getNews(q){
   fetch(`https://newsapi.org/v2/everything?q=${q}&sortBy=publishedAt&apiKey=159ab043c8be4a0d9f4f385a853ef619`)
   .then((res) => {
      return res.json();
   })
   .then((data) => {
      // console.log(data);
      let content = data.articles.map(createArticle)
      news.innerHTML = content.join("\n")

   })
   
}

async function getNews2(q) {
   let res = await fetch(`https://newsapi.org/v2/everything?q=${q}&sortBy=publishedAt&apiKey=159ab043c8be4a0d9f4f385a853ef619`)
   let content = await res.json();
   content = content.articles.map(createArticle)
   updateUI(content.join("\n"));
   
   // console.log(content);
}

function createArticle(article, i){
   return `
      <article id=${i}>
         <div>
            <img src="${article.urlToImage}" alt="">
         </div>
         <div id="info">
            <h3>${article.title}</h3>
            <p>${article.description}</p>
            <time>${article.publishedAt}</time>
         </div>
         <div id="voter">
            <i id="${i}-up" onClick="document.getElementById(${i}+'-counter').innerText =parseInt(document.getElementById(${i}+'-counter').innerHTML)+1;" class="fas fa-sort-up"></i>
            <span id="${i}-counter">${count_vote}</span>
            <i id="${i}-down" onClick="document.getElementById(${i}+'-counter').innerText =parseInt(document.getElementById(${i}+'-counter').innerHTML)-1;"  class="fas fa-sort-down"></i>
          </div>
      </article>`;
}

function updateUI(content){
   news.innerHTML = content;
}

function changeCountVote(i, type){
   // count_vote = (type == 'up')? count_vote++: count_vote--
   // if (type == 'up') {
   //    count_vote++;
   //    // var value = document.getElementById(i+'-counter').innerHTML;
   //    // // document.getElementById(i+'-counter').innerText = value++;
   //    // console.log(value);      
   //    console.log(count_vote);      
   // } else {
   //    count_vote--;
   //    console.log(count_vote);
   // }
}